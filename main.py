import logging
import sqlite3
from datetime import datetime

import requests
import xmltodict


def set_logging():
    logging.basicConfig(level=logging.INFO, format="%(asctime)s [%(levelname)s] %(message)s",
                        handlers=[logging.FileHandler("debug.log"), logging.StreamHandler()])


class CbrRatesApi:
    HEADERS = {}

    def __init__(self, url):
        self.url = url

    def set_header(self, name, value):
        self.HEADERS[name] = value
        return self.HEADERS

    @staticmethod
    def set_payload(ondate: str = datetime.strftime(datetime.today(), '%Y-%m-%d')):
        return f"""<?xml version="1.0" encoding="utf-8"?>
        <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
        xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
          <soap12:Body>
            <GetCursOnDateXML xmlns="http://web.cbr.ru/">
              <On_date>{ondate}</On_date>
            </GetCursOnDateXML>
          </soap12:Body>
        </soap12:Envelope>"""

    def get_rates(self):
        resp = requests.post(api.url,
                             data=self.set_payload(),
                             headers=self.set_header('content-type', 'application/soap+xml'))
        resp.raise_for_status()
        return resp.content


class DbApi:

    def __init__(self, db_name):
        self.connection = self.connect(db_name)

    @staticmethod
    def connect(db_name):
        return sqlite3.connect(db_name)

    @staticmethod
    def create_tables(cursor):

        cursor.execute("""CREATE TABLE IF NOT EXISTS currency_order(
                        id INTEGER  PRIMARY KEY,
                        on_date     TEXT UNIQUE)""")
        cursor.execute("""CREATE TABLE IF NOT EXISTS currency_rates(
                        order_id    INTEGER,
                        code        INTEGER NOT NULL,
                        code_name   TEXT NOT NULL,
                        name TEXT   NOT NULL,
                        scale INT   NOT NULL,
                        rate TEXT   NOT NULL,
                        FOREIGN KEY (order_id) REFERENCES currency_order(id))""")

    @staticmethod
    def set_order_rate(cursor, on_date):

        try:
            datetime.strptime(on_date, '%Y%m%d')
            order_id = cursor.execute(
                "INSERT INTO currency_order (on_date) values(?) returning id;", (on_date,)
            ).fetchone()[0]
            return order_id

        except (ValueError, TypeError):
            raise

    @staticmethod
    def get_order_rate(cursor, on_date):
        try:
            datetime.strptime(on_date, '%Y%m%d')
        except (ValueError, TypeError):
            raise
        row = cursor.execute("SELECT * FROM currency_order o WHERE o.on_date=?", (on_date,)).fetchone()
        return row[0]


if __name__ == '__main__':

    try:
        api = CbrRatesApi("https://cbr.ru/DailyInfoWebServ/DailyInfo.asmx")
        xml = api.get_rates()

        data = xmltodict.parse(xml)
        rates = data['soap:Envelope']['soap:Body']['GetCursOnDateXMLResponse']['GetCursOnDateXMLResult']['ValuteData']

        set_logging()
        logging.info('Подключаемся к БД...')
        # conn = sqlite3.connect('rates.db')
        conn = DbApi.connect('rates.db')
        cur = conn.cursor()

        logging.info("Создаем таблицы если их нет...")
        DbApi.create_tables(cur)
        logging.info("Добавляем распоряжение")
        try:
            order_id = DbApi.set_order_rate(cur, rates['@OnDate'])
        except sqlite3.IntegrityError as err:
            logging.warning(f'Курс за данную дату {rates["@OnDate"]} уже пристуствует в БД')
            order_id = DbApi.get_order_rate(cur, rates['@OnDate'])

        for currency in rates['ValuteCursOnDate']:
            rate = (order_id, int(currency['Vcode']), currency['VchCode'], currency['Vname'], int(currency['Vnom']),
                    currency['Vcurs'])

            if int(currency['Vcode']) in (156, 392, 840, 978, 398):
                cur.execute("INSERT INTO currency_rates values(?,?,?,?,?,?)", rate)

        rows = cur.execute(
            "SELECT o.id, o.on_date, r.code, r.code_name, r.name, r.scale, r.rate FROM currency_order o join currency_rates r on o.id = r.order_id where o.on_date = ?",
            (on_date,)).fetchall()
        print(f"Номер распоряжения, Дата установки, Цифровой код валюты, ")
        for row in rows:
            print(*row)

        conn.commit()
    except Exception as err:
        logging.error(repr(err))
        conn.rollback()
